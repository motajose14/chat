	const path= require('path');
	const express= require('express');
	const app= express();
	const SocketIO= require('socket.io');


	//settings

	app.set('port', process.env.PORT || 3000);

	//statis files

	app.use(express.static(path.join(__dirname, 'public')));


	

	//start the server

	const server = app.listen(app.get('port'), ()=>{
		console.log('Server on port:', app.get('port'));
	});

	//websockets

	const io= SocketIO(server);


	io.on('connection', (socket)=>{
		console.log('New connection');

		socket.on('chat:message', (data)=>{
			//message.push(data);
			io.sockets.emit('chat:message', data);
		});

		socket.on('chat:typing', (data)=>{
			//message.push(data);
			socket.broadcast.emit('chat:typing', data);
		});
	});


	